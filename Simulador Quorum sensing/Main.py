import pygame,sys, random, math, cv2, os, time
import cv2 as cv
from random import randint
from PyQt5 import uic, QtWidgets,QtCore, QtGui
from PyQt5.QtWidgets import QFileDialog, QApplication, QMainWindow, QMessageBox, QAction , QDialog, QLabel, QPushButton
from PIL import Image
import numpy as np
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal
# from openpyxl import Workbook
from Mainmenu2 import Ui_Resultados

qtCreatorFile = "Mainmenu.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class MyApp(QtWidgets.QMainWindow,Ui_MainWindow):

	def __init__(self):
		QtWidgets.QMainWindow.__init__(self)
		Ui_MainWindow.__init__(self)
		self.setupUi(self)
		self.Simulacion.clicked.connect(self.simular)
		self.abrir.clicked.connect(self.cargar)
		self.guardar.clicked.connect(self.GuardarCambios)
		self.acerca.triggered.connect(self.menuacercade)
		self.ayuda.triggered.connect(self.menuayuda)
		
		##variables fijas de seleccion--------------------------------------------------------------
		
		self.Width.setValidator(QtGui.QDoubleValidator())
		self.Length.setValidator(QtGui.QDoubleValidator())
		self.Nrobots.setValidator(QtGui.QDoubleValidator())
		self.C1_1.setValidator(QtGui.QDoubleValidator())
		self.C1_2.setValidator(QtGui.QDoubleValidator())
		self.C1_3.setValidator(QtGui.QDoubleValidator())
		self.C1_4.setValidator(QtGui.QDoubleValidator())
		self.C2_1.setValidator(QtGui.QDoubleValidator())
		self.C2_2.setValidator(QtGui.QDoubleValidator())
		self.C2_3.setValidator(QtGui.QDoubleValidator())
		self.C2_4.setValidator(QtGui.QDoubleValidator())
		self.C3_1.setValidator(QtGui.QDoubleValidator())
		self.C3_2.setValidator(QtGui.QDoubleValidator())
		self.C3_3.setValidator(QtGui.QDoubleValidator())
		self.C3_4.setValidator(QtGui.QDoubleValidator())
		self.C4_1.setValidator(QtGui.QDoubleValidator())
		self.C4_2.setValidator(QtGui.QDoubleValidator())
		self.C4_3.setValidator(QtGui.QDoubleValidator())
		self.C4_4.setValidator(QtGui.QDoubleValidator())
		self.Qsensing.setValidator(QtGui.QDoubleValidator())
		self.Velocidad.setValidator(QtGui.QDoubleValidator())
		
	global robot_speed_x, robot_speed_y, robot
	global matrizb
		##------------------------------------------------------------------------------------------

	def menuacercade(self):
		QMessageBox.information(self,"Acerca de...","Simulador Path Planning: Quorum Sensing\n \nAutores :\nEyder A. Rodríguez C.\nDaniel M. Romero S.\nFredy H. Martínez S.\n \nGrupo de Investigación ARMOS\n \nUniversidad Distrital Fancisco José De Caldas",QMessageBox.Ok)

	def menuayuda(self):
		QMessageBox.information(self,"Ayuda","-La cantidad de robots dependerá del tamaño asigando al ambiente cargado.\n-Todas las imagenes que se van a cargar al simulador deben ser fondo blanco y no poseer margenes.\n-Se cuenta con un robot de dimension 0.6m x 0.6m  y con una velocidad no superior a 10 m/s .\n-La simulación correrá si y solo si, se ha cargado un ambiente propicio para la simulación.",QMessageBox.Ok)	

	def cargar(self):
		filename = QFileDialog.getOpenFileName(self,'Selecciona tu imagen :','D:/',("Image Files (*.jpg *.png)"))
		global ruta
		ruta = filename[0]
		global path,newImg
		path = ruta.replace(os.sep, '/')
		np.set_printoptions(threshold=np.inf)
		print (path)
		##cambio a escala de grises
		im_gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
		## por el metodo otsu vuelve la matriz a valores binarios
		(thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
		thresh = 120
		im_bw = cv2.threshold(im_gray, thresh, 255, cv2.THRESH_BINARY)[1]
		##escalamos la imagen ingresada a un tamaño de 800x600
		newImg = cv2.resize(im_bw, (240, 240))

		## conteo de los pixeles blancos de la imagen
		pixW=cv2.countNonZero(newImg)
		
		cv2.imwrite('1.jpg', newImg)
		self.guardar.setEnabled(True);

	def GuardarCambios(self):
		
		global  sensor_range,lengthR, widthR, velocidad, length, width, nrobots, qsensing, c11, c12, c13, c14,c21,c22,c23,c24,c31,c32,c33,c34,c41,c42,c43,c44
		self.Photo.setPixmap(QtGui.QPixmap('1.jpg'))
		width=int(self.Width.text())
		length=int(self.Length.text())
		nrobots=int(self.Nrobots.text())
		qsensing=int(self.Qsensing.text())
		velocidad=int(self.Velocidad.text())

		c11=int(self.C1_1.text())
		c12=int(self.C1_2.text())
		c13=int(self.C1_3.text())
		c14=int(self.C1_4.text())
		c21=int(self.C2_1.text())
		c22=int(self.C2_2.text())
		c23=int(self.C2_3.text())
		c24=int(self.C2_4.text())
		c31=int(self.C3_1.text())
		c32=int(self.C3_2.text())
		c33=int(self.C3_3.text())
		c34=int(self.C3_4.text())
		c41=int(self.C4_1.text())
		c42=int(self.C4_2.text())
		c43=int(self.C4_3.text())
		c44=int(self.C4_4.text())

		##Condicional cantidad de robots 
		
		area=int(width*length)
		dimension=int(width)
		crobots= 800*0.91## comparar 0.3721 
		drobots=nrobots * 800/width * 1.41

		if drobots>crobots:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Demasiados robots")
			msg.setInformativeText('La cantidad de robots no es permitida para el tamaño del ambiente')
			msg.setWindowTitle("Error")
			msg.exec_()		
		##Condicional tamaño ambiente
		if area>40000:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error tamaño del ambiente")
			msg.setInformativeText('El area debe ser inferior a 40000 m²')
			msg.setWindowTitle("Error")
			msg.exec_()
		if area<400:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error tamaño del ambiente")
			msg.setInformativeText('El valor debe ser superior a 400 m² ')
			msg.setWindowTitle("Error")
			msg.exec_()
		##Condicional velocidad robot
		if velocidad>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error velocidad del robot")
			msg.setInformativeText('La velocidad del robot debe ser menor a 10 m/s')
			msg.setWindowTitle("Error")
			msg.exec_()
		if velocidad<2:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText(" Error velocidad del robot")
			msg.setInformativeText('La velocidad del robot debe ser superior a 2 m/s  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		##condicionales Landmarks
		if c11>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (1,1) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c12>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (1,2) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c13>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (1,3) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c14>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (1,4) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c21>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (2,1) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c22>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (2,2) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c23>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (2,3) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c24>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (2,4) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c31>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (3,1) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c32>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (3,2) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c33>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (3,3) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c34>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilla (3,4) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c41>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casillas (4,1) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c42>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error (Casillas (4,2) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c43>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casillas (4,3) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()
		if c44>10:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error Casilas (4,4) ")
			msg.setInformativeText('El valor del peso debe ser menor a 10  ')
			msg.setWindowTitle("Error")
			msg.exec_()

		self.Simulacion.setEnabled(True);

	def simular(self):
		global pos_x2, pos_y2, inicio_quorum_final, hg, waiting_robots_entero, b2, waiting_robots, timer_inicio,cerrar_pg, b, f1, hi, hi2, r1, d1, umbral, inicio_simulacion, robot_speed, robot_speed_x, robot_speed_y, robot, c, e, num_quo, lalo, pl, plo, pli, a1, infected_robots_entero, r, not_infected_robots, w, y, m, s, not_infected_robots2, division_umbral, cell_weight, cell_time, z, inicio_quorum, final_quorum, x, timer, umbral_sim
		timer_inicio= time.time()-3

		def ball_animation(): #Definición que determina el movimiento de los robots y sus colisiones
			global pos_x2, pos_y2, inicio_quorum_final, hg, waiting_robots_entero, b2, waiting_robots, timer_final, b, f1, hi, hi2, r1, d1, umbral, inicio_simulacion, robot_speed, robot_speed_x, robot_speed_y, robot, c, e, num_quo, lalo, pl, plo, pli, a1, infected_robots_entero, r, not_infected_robots, w, y, m, s, not_infected_robots2, division_umbral, cell_weight, cell_time, z, inicio_quorum, final_quorum, x, timer, umbral_sim,c8
			
			for t1 in range(0,td):
				timer[t1] = time.time()

			pos_x = math.ceil(random.uniform(-robot_speed*2**(1/2),0)) #cálculo al azar para obtener un ángulo al azar teniendo en cuenta la velocidad del robot
			pos_y = math.ceil((2*robot_speed**2-pos_x**2)**(1/2)) # cálculo de la otra componente del vector
			pos_x2 = math.ceil(random.uniform(-robot_speed*2**(1/2),0.99)) #cálculo al azar para obtener un ángulo al azar teniendo en cuenta la velocidad del robot
			pos_y2 = math.ceil((2*robot_speed**2-pos_x**2)**(1/2)) # cálculo de la otra componente del vector
			a =  1 if random.random() < 0.5 else -1 #Valor al azar entre 1 ó -1
			
			for c in range(0,number_of_robots): #Ciclo para mover a todos los robots
				timer_final=time.time()
				if (timer[0] - inicio_simulacion[0]) >= 3:

					robot[c].x += robot_speed_x[c] #Asigna la posición x de un robot dependiendo de la velocidad
					robot[c].y += robot_speed_y[c] #Asigna la posición y de un robot dependiendo de la velocidad
					sensor[c].x += robot_speed_x[c] #Asigna la posición x de los sensores dependiendo de la velocidad
					sensor[c].y += robot_speed_y[c] #Asigna la posición y de los sensores dependiendo de la velocidad


				collision_tolerance = 10

				for e in range(1,number_of_robots):
					if sensor[c].colliderect(sensor[e]) and c != e:
						if abs(sensor[c].top - sensor[e].bottom) < collision_tolerance and robot_speed_y[e] > 0 and robot_speed_y[c] < 0:
							robot_speed_x[c] = a*pos_y
							robot_speed_y[c] = -pos_x
							robot_speed_x[e] = a*pos_y
							robot_speed_y[e] = pos_x
						if abs(sensor[c].bottom - sensor[e].top) < collision_tolerance and robot_speed_y[e] < 0 and robot_speed_y[c] > 0:
							robot_speed_x[c] = a*pos_y
							robot_speed_y[c] = -pos_x
							robot_speed_x[e] = a*pos_y
							robot_speed_y[e] = pos_x
						if abs(sensor[c].right - sensor[e].left) < collision_tolerance and robot_speed_x[e] < 0 and robot_speed_x[c] > 0:
							robot_speed_x[c] = pos_x
							robot_speed_y[c] = a*pos_y
							robot_speed_x[e] = -pos_x
							robot_speed_y[e] = a*pos_y
						if abs(sensor[c].left - sensor[e].right) < collision_tolerance and robot_speed_x[e] > 0 and robot_speed_x[c] < 0:
							robot_speed_x[c] = -pos_x
							robot_speed_y[c] = a*pos_y
							robot_speed_x[e] = pos_x
							robot_speed_y[e] = a*pos_y

				for b in range(len(objects)):
					if collideRectPolygon(sensor[c], objects[b]):
						
						# robot_speed_y[c] *= -1
						# robot_speed_x[c] *= -1
				# 		if 	robot_speed_y[c] < 0 :
				# 			robot_speed_y[c] *= -1
				# 			# robot_speed_x[c] = a*pos_y
				# 			robot[c].y += 4
				# 			sensor[c].y += 4

				# # for b in range(len(objects)):
				# # 	if collideRectPolygon(sensor[c], objects[b]):
				# 		if  robot_speed_y[c] > 0 :
				# 			robot_speed_y[c] *= -1
				# 			# robot_speed_x[c] = a*pos_y
				# 			robot[c].y -= 4
				# 			sensor[c].y -= 4

				# # for b in range(len(objects)):
				# # 	if collideRectPolygon(sensor[c], objects[b]):

				# 		if 	robot_speed_x[c] > 0 :
				# 			robot_speed_x[c] *= -1
				# 			# robot_speed_y[c] = a*pos_y
				# 			robot[c].x -= 4
				# 			sensor[c].x -= 4
			
				# # for b in range(len(objects)):
				# # 	if collideRectPolygon(sensor[c], objects[b]):

				# 		if  robot_speed_x[c] < 0 :
				# 			robot_speed_x[c] *= -1
				# 			# robot_speed_y[c] = a*pos_y
						# robot[c].x -= 4
						# sensor[c].x -= 4

						# else:
						if robot_speed_y[c] > 0:
							robot_speed_y[c] = -pos_y2
						else:
							robot_speed_y[c] = pos_y2

						if robot_speed_x[c] > 0:
							robot_speed_x[c] = pos_x2
						else:
							robot_speed_x[c] = -pos_x2

				#Detección robots cuadrícula
				for ch in range(0,hd):
					for ch1 in range(0,vd):
						if rect[0][ch] < sensor[c].x < rect[0][ch+1] and rect[1][ch1] < sensor[c].y < rect[1][ch1+1]: #verifica que el sensor del robot esté dentro de una cuadrícula dada
							for po in range(0 ,hd):
								if ch1 == 0 and ch == po:
									num_quo[c][ch] = 1
								else:
									num_quo[c][po] = 0

							for po in range(0 ,hd):
								if ch1 == 1 and ch == po:
									num_quo[c][ch+hd] = 1
								else:
									num_quo[c][po+4] = 0

							for po in range(0 ,hd):
								if ch1 == 2 and ch == po:
									num_quo[c][ch+hd*2] = 1
								else:
									num_quo[c][po+8] = 0

							for po in range(0 ,hd):
								if ch1 == 3 and ch == po:
									num_quo[c][ch+hd*3] = 1
								else:
									num_quo[c][po+12] = 0

							for pl in range(0, td): #Ciclo de 0 a la cantidad de divisiones para almacenar la cantidad de robots por división
								for pl1 in range(0,number_of_robots): # ciclo de 0 a la cantidad de robots
									pli[pl1] = num_quo[pl1][pl] #Guarda la cantidad de robots que hay en cada división #ARREGLAR PARA N ROBOTS
								plo[pl] = sum(pli) #Guarda la cantidad de robots después del ciclo para siempre mostrar la cantidad en tiempo real

			for d1 in range(td):

				if (timer[d1] - inicio_simulacion[d1]) >= 9: # EL 6 ES EL TIEMPO QUE TARDA EN INICIAR Y REINICIAR EL QUORUM
					for b3 in range(td):
						if plo[b3] >= qsensing and cell_weight[b3] != 0:
							casillas_quorum[b3] = plo[b3]
							for b2 in range(number_of_robots):
								if num_quo[b2][b3] == 1:
									waiting_robots[b2]= 1 #[plo.index(plo[b3])]
					if plo[d1] > a1[d1] and cell_weight[d1] != 0:
						hi.remove(d1)
						for f in range(0,number_of_robots):

							if num_quo[f][d1] == 1 and infected_robots[hi[0]][f] != 1 and infected_robots[hi[1]][f] != 1 and infected_robots[hi[2]][f] != 1 and infected_robots[hi[3]][f] != 1 and infected_robots[hi[4]][f] != 1 and infected_robots[hi[5]][f] != 1 and infected_robots[hi[6]][f] != 1 and infected_robots[hi[7]][f] != 1 and infected_robots[hi[8]][f] != 1 and infected_robots[hi[9]][f] != 1 and infected_robots[hi[10]][f] != 1 and infected_robots[hi[11]][f] != 1 and infected_robots[hi[12]][f] != 1 and infected_robots[hi[13]][f] != 1 and infected_robots[hi[14]][f] != 1:
								infected_robots[d1][f] = 1
								
							else:
								infected_robots[d1][f] = 0
						hi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

						inicio_quorum[d1] = time.time()
						division_umbral[d1] = d1

						a1[d1] += 1						
						while hg == 0:
							inicio_quorum_final = inicio_quorum[d1]
							hg +=1

				if a1[d1] < umbral+1:
					final_quorum[d1] = time.time()
					# waiting_robots[d1] = plo[b3]
#Cambiar el color de los robots rojos a azules y cuando se cumpla el quorum en algun lado ponerlos en otra variable y dibujarlos rojos
				if (final_quorum[d1] - inicio_quorum[d1]) > cell_weight[d1]*10:
					if cell_weight[d1] != 0: #EL 10 ES EL NÚMERO DE SEGUNDOS POR CADA PESO DE 1
						a1[d1] = umbral_sim[d1]
						inicio_quorum[d1] = 9999999999999999999999
						final_quorum[d1] = 0
						timer[d1] = 0
						inicio_simulacion[d1] = time.time()
						z[d1] = 16
						for w in range(number_of_robots):
							infected_robots[d1][w] = 0

			not_infected_robots = not_infected_robots2.copy()
			for d1 in range(td):
				infected_robots_entero[d1] = infected_robots[d1].copy()

				for m in range(0,number_of_robots):
					infected_robots_entero[d1][m] = infected_robots[d1][m]*m

				if infected_robots[d1][0] == 1:
					for y in range(number_of_robots-sum(infected_robots[d1])+1):
						infected_robots_entero[d1].remove(0)

					infected_robots_entero[d1].insert(0,0)

				else:
					for y in range(number_of_robots-sum(infected_robots[d1])):
						infected_robots_entero[d1].remove(0)

				for s in range(0, len(infected_robots_entero[d1])):
					not_infected_robots.remove(infected_robots_entero[d1][s])		

			waiting_robots_entero = waiting_robots.copy()

			for m in range(0,number_of_robots):
				waiting_robots_entero[m] = waiting_robots_entero[m]*m

			if waiting_robots[0] == 1:
				for y in range(number_of_robots-sum(waiting_robots)+1):
					waiting_robots_entero.remove(0)

				waiting_robots_entero.insert(0,0)

			else:
				for y in range(number_of_robots-sum(waiting_robots)):
					waiting_robots_entero.remove(0)

			# for s in range(0, len(infected_robots_entero[d1])):
			# 	not_infected_robots.remove(infected_robots_entero[d1][s])	
				

			for r1 in range(td):

				g = division_umbral[r1]	
				for r in (infected_robots_entero[r1]):

					if division_umbral[r1] == 0 or division_umbral[r1] == 1 or division_umbral[r1] == 2 or division_umbral[r1] == 3:

						if sensor[r].top <= rect[1][0]+10:
							robot[r].y += 4
							sensor[r].y += 4
							robot_speed_y[r] = -pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].bottom >= rect[1][1]-10:
							robot[r].y -= 4
							sensor[r].y -= 4
							robot_speed_y[r] = pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].left <= rect[0][g]+10:
							robot[r].x += 4
							sensor[r].x += 4
							robot_speed_x[r] = -pos_x
							robot_speed_y[r] = a*pos_y

						if sensor[r].right >= rect[0][g+1]-10:
							robot[r].x -= 4
							sensor[r].x -= 4
							robot_speed_x[r] = pos_x
							robot_speed_y[r] = a*pos_y

					if division_umbral[r1] == 4 or division_umbral[r1] == 5 or division_umbral[r1] == 6 or division_umbral[r1] == 7:

						if sensor[r].top <= rect[1][1]+10:
							robot[r].y += 4
							sensor[r].y += 4
							robot_speed_y[r] = -pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].bottom >= rect[1][2]-10:
							robot[r].y -= 4
							sensor[r].y -= 4
							robot_speed_y[r] = pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].left <= rect[0][g-4]+10:
							robot[r].x += 4
							sensor[r].x += 4
							robot_speed_x[r] = -pos_x
							robot_speed_y[r] = a*pos_y

						if sensor[r].right >= rect[0][g-3]-10:
							robot[r].x -= 4
							sensor[r].x -= 4
							robot_speed_x[r] = pos_x
							robot_speed_y[r] = a*pos_y

					if division_umbral[r1] == 8 or division_umbral[r1] == 9 or division_umbral[r1] == 10 or division_umbral[r1] == 11:

						if sensor[r].top <= rect[1][2]+10:
							robot[r].y += 4
							sensor[r].y += 4
							robot_speed_y[r] = -pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].bottom >= rect[1][3]-10:
							robot[r].y -= 4
							sensor[r].y -= 4
							robot_speed_y[r] = pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].left <= rect[0][g-8]+10:
							robot[r].x += 4
							sensor[r].x += 4
							robot_speed_x[r] = -pos_x
							robot_speed_y[r] = a*pos_y

						if sensor[r].right >= rect[0][g-7]-10:
							robot[r].x -= 4
							sensor[r].x -= 4
							robot_speed_x[r] = pos_x
							robot_speed_y[r] = a*pos_y

					if division_umbral[r1] == 12 or division_umbral[r1] == 13 or division_umbral[r1] == 14 or division_umbral[r1] == 15:

						if sensor[r].top <= rect[1][3]+10:
							robot[r].y += 4
							sensor[r].y += 4
							robot_speed_y[r] = -pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].bottom >= rect[1][4]-10:
							robot[r].y -= 4
							sensor[r].y -= 4
							robot_speed_y[r] = pos_x
							robot_speed_x[r] = a*pos_y

						if sensor[r].left <= rect[0][g-12]+10:
							robot[r].x += 4
							sensor[r].x += 4
							robot_speed_x[r] = -pos_x
							robot_speed_y[r] = a*pos_y
						if sensor[r].right >= rect[0][g-11]-10:
							robot[r].x -= 4
							sensor[r].x -= 4
							robot_speed_x[r] = pos_x
							robot_speed_y[r] = a*pos_y

				for w in not_infected_robots:

					if sensor[w].top <= 0:
						robot[w].y += 4
						sensor[w].y += 4
						robot_speed_y[w] = -pos_x
						robot_speed_x[w] = a*pos_y
					if sensor[w].bottom >= screen_height:
						robot[w].y -= 4
						sensor[w].y -= 4
						robot_speed_y[w] = pos_x
						robot_speed_x[w] = a*pos_y
					if sensor[w].left <= 0:
						robot[w].x += 4
						sensor[w].x += 4
						robot_speed_x[w] = -pos_x
						robot_speed_y[w] = a*pos_y
					if sensor[w].right >= screen_width:
						robot[w].x -= 4
						sensor[w].x -= 4
						robot_speed_x[w] = pos_x
						robot_speed_y[w] = a*pos_y

		# General setup
		pygame.init()
		clock = pygame.time.Clock()

		# Main Window
		screen_width = 800 #Las dimensiones de la ventana emergente siempre serán 800x600
		screen_width2 = 800
		screen_height = 600
		screen_height2 = 700
		screen = pygame.display.set_mode((screen_width2,screen_height2))
		pygame.display.set_caption('Quorum sensing')

		#Robot setup
		number_of_robots = nrobots #Cantidad de robots
		casillas_quorum = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] #Almacen la cantidad de robots en los lugares que se cumplió el quorum
		umbral = qsensing-1
		a1 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] # Debe haber al menos 1 robot en la zona de interés para que se quede, ayuda con el condicional que lo activa
		final_quorum = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] #Tiempo final de quorum
		inicio_quorum = [9999999999999999]
		for f1 in range(0,16):
			inicio_quorum.append(9999999999999999) #Tiempo inicial de quorum
		z = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] #Constante para que el primer robot sea 1 y no 0
		division_umbral= []
		for q1 in range(16):
			division_umbral.append([]) #Variable para guardar en dónde se completó el umbral
		cerrar_pg = 0
		hg = 0
		inicio_quorum_final = 0
		inicio_quorum_final2 = 0
		g = 0 #Constante para el cliclo que limita el moviemiento de los robots al cumplirse el quorum
		lalo = 0 #Constante de pruebas, borrar después
		e = 0 #Constante para el cliclo for que verifica si se cumplió el quorum
		umbral_sim = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] #Umbral de quorum, aún no se usa en nada
		ambient_width_input = width #Ancho del ambiente en metros dado por el usuario
		ambient_height_input = length #Largo del ambiente en metros dado por el usuario
		ambient_width = int(screen_width/ambient_width_input) #Ancho del ambiente en píxeles/metro
		ambient_height = int(screen_height/ambient_height_input) #Largo del ambiente en píxeles/metro
		robot_speed = velocidad#Velocidad de robot ingresada por el usuario
		robot_width = 0.61 #Dimensiones del robot en metros
		robot_height = 0.61 #Dimensiones del robot en metros
		robot_width = robot_width*ambient_width #Dimensiones del robot en píxeles/metros
		robot_height = robot_height*ambient_width #Dimensiones del robot en píxeles/metros
		robot_width = math.ceil(robot_width) #redondea hacia arriba las dimensiones del robot para que sea un número entero de pixeles
		robot_height = math.ceil(robot_height) #redondea hacia arriba las dimensiones del robot para que sea un número entero de pixeles
		b = 0 #Constante para ayudar con el espaciamiento de los robots y no aparezcan uno encima de otro al iniciar la simulación
		robot = [] #Matriz donde se almacenarán y plotearán los robots
		sensor = [] #Matriz donde se almacenarán y plotearán los sensores de los robots
		sensor_range = 0.8+0.61 #rango del sensor en metros
		sensor_range *= ambient_width
		robot_number = 0 # Cantidad para agregar al ciclo while para que los robots no salgan uno encima de otro
		robot_speed_x = [] #Matriz creada para almacenar la velocidad en x de los robots, todos los robots se mueven a la misma velocidad
		robot_speed_y = [] #Matriz creada para almacenar la velocidad en y de los robots, todos los robots se mueven a la misma velocidad
		objects = [] #Matriz creada para almacenar las figuras obtenidas de la imagen cargada
		timer = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		hi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
		hi2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

		################################ Niveles cuadrícula (Pesos) #######################################
		cell_time = []
		cell_weight = [c11,c12,c13,c14,c21,c22,c23,c24,c31,c32,c33,c34,c41,c42,c43,c44, 1] #Pesos de las celdas, para 16 celdas, EL ÚLTIMO 1 SÓLO SIRVE COMO STOP
		for infected_robots_entero in range(0, len(cell_weight)): #Crea una matriz con la longitud de cell_weigth
			cell_time.append(cell_weight[infected_robots_entero]*60)

		################## Game text #########################
		font = pygame.font.SysFont("jevabusans", 18) #Tamaño y fuente de la letra
		font2 = pygame.font.SysFont("jevabusans", 30)
		font3 = pygame.font.SysFont("jevabusans", 40)
		font4 = pygame.font.SysFont("jevabusans", 20)
		font5 = pygame.font.SysFont("jevabusans", 14)
		text = font.render("0", True, (200,200,200)) #asigna el color gris a las letras RGB
		text2 = font2.render("Finalizar", True, (255,255,255))
		image_resul = pygame.image.load("1.jpg").convert() 
		blanco = pygame.image.load("blanco.png").convert() 
		rojo = pygame.image.load("rojo.png").convert() 
		naranja = pygame.image.load("naranja.png").convert() 
		amarillo = pygame.image.load("amarillo.png").convert() 
		lima = pygame.image.load("lima.png").convert() 
		verde = pygame.image.load("verde.png").convert() 


		#Game text

		font = pygame.font.SysFont("jevabusans", 18)
	
		q_cont = 0
		text_x = []
		text_y = []
		for q in range(0,math.ceil(ambient_width_input),math.ceil(ambient_width_input/10)):
			q = str(q)
			text_x.append(font.render(q, True, (200,200,200)))
			
		print("texto eje x",text_x)

		for q in range(0,math.ceil(ambient_height_input),math.ceil(ambient_height_input/10)): 
			q = str(q)
			text_y.append(font.render(q, True, (200,200,200)))
			
		################## Carga de imagen y obtención de los contornos de la misma ##########################
		im = cv.imread('1.jpg') #Lee la imagen entre los paréntesis, puede ser jpg o png, debe tener fondo blanco
		im_resized = cv.resize(im,(800,600))
		imgray = cv.cvtColor(im_resized, cv.COLOR_BGR2GRAY) #obtiene la imagen cargada en escala de grises
		ret, thresh = cv.threshold(imgray, 127, 255, cv.THRESH_BINARY_INV)# filtra los grises en (127, 255, 0) y devuelve una imagen en blanco y negro
		contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE) #Encuentra los contornos de la imagen
		for a in range(0,len(contours)):
			objects.append([])
			for c1 in range(len(contours[a])):
				objects[a].append([])
				objects[a][c1] = tuple(contours[a][c1][0])
		for a in range(len(contours)):
			objects[a].append(objects[a][0])


		def collideLineLine(l1_p1, l1_p2, l2_p1, l2_p2):

			# normalized direction of the lines and start of the lines
			P  = pygame.math.Vector2(*l1_p1)
			line1_vec = pygame.math.Vector2(*l1_p2) - P
			R = line1_vec.normalize()
			Q  = pygame.math.Vector2(*l2_p1)
			line2_vec = pygame.math.Vector2(*l2_p2) - Q
			S = line2_vec.normalize()

			# normal vectors to the lines
			RNV = pygame.math.Vector2(R[1], -R[0])
			SNV = pygame.math.Vector2(S[1], -S[0])
			RdotSVN = R.dot(SNV)
			if RdotSVN == 0:
				return False

			# distance to the intersection point
			QP  = Q - P
			t = QP.dot(SNV) / RdotSVN
			u = QP.dot(RNV) / RdotSVN

			return t > 0 and u > 0 and t*t < line1_vec.magnitude_squared() and u*u < line2_vec.magnitude_squared()

		def colideRectLine(rect, p1, p2):
			return (collideLineLine(p1, p2, rect.topleft, rect.bottomleft) or
					collideLineLine(p1, p2, rect.bottomleft, rect.bottomright) or
					collideLineLine(p1, p2, rect.bottomright, rect.topright) or
					collideLineLine(p1, p2, rect.topright, rect.topleft))

		def collideRectPolygon(rect, polygon):
			for i in range(len(polygon)-1):
				if colideRectLine(rect, polygon[i], polygon[i+1]):
					return True
			return False

		#################### Colors ##########################

		blue = (0,0,200) #valor en RGB del color azul
		light_grey = (200,200,200) #valor en RGB del color gris
		transparent_grey= ((250,0,0,0)) #valor en RGB del color gris transparente
		green = (0,128,0) #verde
		bg_color = pygame.Color('grey12') #Color del fondo

		########################################## Game divisions ######################################
		abc = 1
		c8 = 0 
		co = 0 #Contador
		i = 0 #Contador
		hd = 4 #Horizontal divisions
		vd = 4 #Vertical divisions
		rect = [[],[]] #Rectangles
		td = hd*vd #Total divisions
		waiting_robots = []
		waiting_robots_entero = []
		num_quo = [] #Variable para almacenar cuántos robots hay en cada celda, arroja la celda y la cantidad de robots en ella
		infected_robots = [] #Variable para determinar qué robots están infectados y ya no pueden salir del punto de interés
		infected_robots_entero = [] #Variable para determinar qué robots están infectados y ya no pueden salir del punto de interés en números enteros
		not_infected_robots = []
		not_infected_robots2 = [] #Variable para almacenar los robots que no están infectados
		plo = [] #Variable para almacenar la cantidad de robots en cada celda

		###### TOMA DE TIEMPOS ######

		inicio_simulacion = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		for t1 in range(0,td): #toma el tiempo del inicio de la simulación 16 veces, una para cada división
			inicio_simulacion[t1] = time.time()
		inicio_simulacion2 = time.time() #Toma el tiempo al inicio de la simulación para entregarlo al final

		pli = [] #Variable para ayudar a almacenar la cantidad de robots en cada celda
		if number_of_robots > 16:
			for lll in range(0,number_of_robots):
				plo.append(0)
		else:
			plo = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		for y1 in range(16):
			infected_robots.append([])
			infected_robots_entero.append([])

			for y2 in range(number_of_robots):
				infected_robots[y1].append(0)  #Number of robots in a selected area
				infected_robots_entero[y1].append(0)

		for lll in range(0,number_of_robots):
			waiting_robots.append(0)
			waiting_robots_entero.append(0)
			num_quo.append([0])
			pli.append(0)
			not_infected_robots2.append(lll)
			not_infected_robots.append(lll)
			for ll in range(0,td-1):
				num_quo[lll].append(0)

		for co in range(math.ceil(screen_width/hd),math.ceil(screen_width),math.ceil(screen_width/hd)): #Crea un ciclo que empieza en m, termina en n y en pasos de p range(m,n,p)
			rect[0].append(co)

		for co in range(math.ceil(screen_height/vd),math.ceil(screen_height),math.ceil(screen_height/vd)):
			rect[1].append(co)

		rect[0].insert(0,0)
		rect[0].append(screen_width)
		rect[1].insert(0,0)
		rect[1].append(screen_height)

		#Crea una cantidad n de robots espaciados entre sí

		while robot_number < number_of_robots:
			robot.append(pygame.Rect(math.ceil(b+robot_height/2), math.ceil(robot_height/2), robot_width, robot_height))
			sensor.append(pygame.Rect(math.ceil(b), 0, math.ceil(sensor_range), math.ceil(sensor_range)))
			robot_speed_x.append(robot_speed)
			robot_speed_y.append(robot_speed)
			b += sensor_range + 5
			robot_number += 1

		#Random start direction of the robots

		def robot_start():
			global d
			for d in range(0,number_of_robots):
				robot[c].x += random.randint(-robot_speed_x,robot_speed_y)
				robot[c].y += random.randint(-robot_speed_x,robot_speed_y)

		########## Divisiones para las celdas###############

		def game_divisions():
			for co in range(math.ceil(screen_width/hd),math.ceil(screen_width),math.ceil(screen_width/hd)):
				pygame.draw.aaline(screen, blue,(co, 0),(co,screen_height), 1)
			for co in range(math.ceil(screen_height/vd),math.ceil(screen_height),math.ceil(screen_height/vd)):
				pygame.draw.aaline(screen, blue,(0, co),(screen_width,co), 1)

		##### Inicio de Pygame, inicia la simulación y permite cerrarla #####
		finalizar = pygame.Rect(400-text2.get_width()/2,600+25,100,50) #Botón de finalizar
		pantalla_principal = True
		pantalla_resultados = False
		while pantalla_principal:
			for event in pygame.event.get():
				if event.type == pygame.MOUSEBUTTONDOWN and event.button==1:
					if finalizar.collidepoint(pygame.mouse.get_pos()):

						pantalla_resultados = True
						pantalla_principal = False

				if event.type == pygame.QUIT:
					pygame.quit()
					sys.exit()

			# Game logic

			ball_animation() #Llama la definición que dicta el movimiento de los robots
			screen.fill(bg_color) #Llena el fondo de color
			game_divisions() #llama la definición que dibuja las divisiones de la simulación
			pygame.draw.aaline(screen, light_grey,(30, 0),(30, 600), 1) #Dibuja el eje x
			pygame.draw.aaline(screen, light_grey,(0, 570),(800,570), 1) #Dibuja el eje y

			if finalizar.collidepoint(pygame.mouse.get_pos()):
				pygame.draw.rect(screen, (237,128,19),finalizar,0)
			else:
				pygame.draw.rect(screen, (70,189,34),finalizar,0)
			screen.blit(text2,(400-text2.get_width()/2+(finalizar.width-text2.get_width())/2,625+(finalizar.height-text2.get_height())/2))
			# obtiene un solo evento de la cola de eventos

			for x in range(30,screen_width,math.ceil((screen_width-30)/10)):
					pygame.draw.aaline(screen, light_grey,(x, screen_height-20),(x,screen_height-40), 1)
					if x >31:
						screen.blit(text_x[q_cont],(x-3, screen_height-20))
					else:
						screen.blit(text_x[0],(x-10, screen_height-28))
					q_cont += 1
			q_cont = 9
			for x in range(57,screen_height,math.ceil((screen_height-30)/10)):
					pygame.draw.aaline(screen, light_grey,(20,x),(40,x), 1)
					if x <screen_height-58:
						screen.blit(text_y[q_cont],(20-8,x-6))
					q_cont -= 1

			for b in not_infected_robots:
				pygame.draw.ellipse(screen, green, sensor[b]) #Dibuja los robots
				pygame.draw.rect(screen, light_grey, robot[b]) #Dibuja los sensores de los robots
			for b in range(0,number_of_robots):
				pygame.draw.ellipse(screen, green, sensor[b]) #Dibuja los robots
				pygame.draw.rect(screen, light_grey, robot[b]) #Dibuja los sensores de los robots


			if len(not_infected_robots) != number_of_robots:
				for b1 in range (td):
					for b in range(len(infected_robots_entero[b1])):
						pygame.draw.ellipse(screen, blue, sensor[infected_robots_entero[b1][b]]) #Dibuja los robots
						pygame.draw.rect(screen, light_grey, robot[infected_robots_entero[b1][b]]) #Dibuja los sensores de los robots

			for b in waiting_robots_entero:

				pygame.draw.ellipse(screen, transparent_grey, sensor[b]) #Dibuja los robots
				pygame.draw.rect(screen, light_grey, robot[b]) #Dibuja los sensores de los robots

			for d in range(0,len(contours)):
				pygame.draw.polygon(screen, light_grey, objects[d]) #Dibuja los objetos

			# print(casillas_quorum)

			pygame.display.flip()
			clock.tick(30) # 30FPS
			final_simulacion2 = time.time()
			
			tiempo_total=timer_final- timer_inicio
			tiempo_total=round(tiempo_total,2)
			inicio_quorum_final2 = inicio_quorum_final - timer_inicio
			inicio_quorum_final2=round(inicio_quorum_final2,2)

		text3 = font2.render('Tiempo total de ejecución:',True, (200,200,200) )
		text4 = font2.render(str(tiempo_total),True, (200,200,200) )
		text5 = font2.render('Cantidad de robots:',True, (200,200,200) )
		text6 = font2.render(str(number_of_robots),True, (200,200,200) )
		text7 = font2.render('Salir',True, (200,200,200) )
		text8 = font2.render('segundos',True, (200,200,200) )
		text11 = font3.render('Resultados',True, (200,200,200) )
		text12 = font5.render('0% - 1%',True, (200,200,200) )
		text13 = font5.render('1% - 20%',True, (200,200,200) )
		text14 = font5.render('20% - 40%',True, (200,200,200) )
		text15 = font5.render('40% - 60%',True, (200,200,200) )
		text16 = font5.render('60% - 80%',True, (200,200,200) )
		text17 = font5.render('80% - 100%',True, (200,200,200) )
		text18 = font2.render('Tiempo en llegar al quorum:',True, (200,200,200) )
		text19 = font2.render(str(inicio_quorum_final2),True, (200,200,200) )
		
		while pantalla_resultados:
			for event in pygame.event.get():
				if event.type == pygame.MOUSEBUTTONDOWN and event.button==1:
					if finalizar.collidepoint(pygame.mouse.get_pos()):

						pygame.quit()
						sys.exit()

				if event.type == pygame.QUIT:
					pygame.quit()
					sys.exit()

			screen.fill(bg_color)
			###########################################################

			if finalizar.collidepoint(pygame.mouse.get_pos()):
				pygame.draw.rect(screen, (237,128,19),finalizar,0)
			else:
				pygame.draw.rect(screen, (70,189,34),finalizar,0)
			screen.blit(text3,(200,400))
			screen.blit(text4,(500,400))
			screen.blit(text8,(570,400))
			screen.blit(text5,(200,450))
			screen.blit(text6,(500,450))
			screen.blit(text18,(200,500))
			screen.blit(text19,(500,500))
			screen.blit(text8,(570,500))
			screen.blit(text7,(385,640))		
			screen.blit(text11,(300,30))
			screen.blit(image_resul,[240,100] )
			pygame.draw.line(screen,(0,0,255),(590,100),(590,340),2)
			pygame.draw.line(screen,(0,0,255),(650,100),(650,340),2)
			pygame.draw.line(screen,(0,0,255),(710,100),(710,340),2)
			pygame.draw.line(screen,(0,0,255),(530,160),(770,160),2)
			pygame.draw.line(screen,(0,0,255),(530,220),(770,220),2)
			pygame.draw.line(screen,(0,0,255),(530,280),(770,280),2)
			pygame.draw.rect(screen,(0,128,0),[75,140,30,20]) ##verde
			pygame.draw.rect(screen,(173,255,47),[75,160,30,20] )##lima
			pygame.draw.rect(screen,(255,255,0),[75,180,30,20] )##amarillo
			pygame.draw.rect(screen,(255,165,0),[75,200,30,20] )##naranja
			pygame.draw.rect(screen,(255,0,0),[75,220,30,20] )##rojo 
			pygame.draw.rect(screen,(255,255,255),[75,240,30,20] )##blanco
			screen.blit(text12,(110,250))
			screen.blit(text13,(110,230))
			screen.blit(text14,(110,210))
			screen.blit(text15,(110,190))
			screen.blit(text16,(110,170))
			screen.blit(text17,(110,150))
			# screen.blit(text18,(110,130))
			# screen.blit(text19,(110,110))
			pygame.draw.line(screen,(0,0,255),(300,100),(300,340),2)
			pygame.draw.line(screen,(0,0,255),(360,100),(360,340),2)
			pygame.draw.line(screen,(0,0,255),(420,100),(420,340),2)
			pygame.draw.line(screen,(0,0,255),(240,160),(480,160),2)
			pygame.draw.line(screen,(0,0,255),(240,220),(480,220),2)
			pygame.draw.line(screen,(0,0,255),(240,280),(480,280),2)

			while c8 < 16: 
				
				casillas_quorum[c8]*= 1/number_of_robots * 100
				c8 += 1
			
			Casillas=casillas_quorum
			#print(casillas_quorum)
			# Casillas=[c11,c12,c13,c14,c21,c22,c23,c24,c31,c32,c33,c34,c41,c42,c43,c44]
			posicion=[[530,100],[590,100],[650,100],[710,100],[530,160],[590,160],[650,160],[710,160],[530,220],[590,220],[650,220],[710,220],[530,280],[590,280],[650,280],[710,280]]
			for i in range(len(Casillas)):
				if Casillas[i] == 0:
					screen.blit(blanco,posicion[i])
				if 1 <= Casillas[i] < 20:
					screen.blit(rojo,posicion[i])
				if 10 <= Casillas[i] < 40:
					screen.blit(naranja,posicion[i])
				if 40 <= Casillas[i] < 60:
					screen.blit(amarillo,posicion[i])
				if 60 <= Casillas[i] < 80:
					screen.blit(lima,posicion[i])
				if 80 <= Casillas[i] < 100:
					screen.blit(verde,posicion[i])

			###########################################################

			pygame.display.flip()
			clock.tick(30) # 30FPS

if __name__=="__main__":
	app = QtWidgets.QApplication(sys.argv)
	window = MyApp()
	window.show()
	sys.exit(app.exec_())
